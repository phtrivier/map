(function(View) {

    describe("view", function() {

        it("converts model to Leaflet's model", function() {

            var v = new View({
                domId: "test"
            });

            var json = {
                "name": "Francazal",
                "id": "francazal-2015",
                "Map": {
                    "world": true,
                    "GoogleMaps": {
                        "viewSettings": {
                            "zoom": 16,
                            "tilt": 0
                        }
                    },
                    "SVG": {
                        "minZoom": 0.2,
                        "maxZoom": 10
                    },
                    "origin": {
                        "lat": 43.539698130087686,
                        "lng": 1.3620845608763283
                    }
                },
                "Lines": [{
                    "color": "#1396A5",
                    "points": [{
                        "lat": 43.537571739831066,
                        "lng": 1.36291393215029
                    }, {
                        "lat": 43.537711013769666,
                        "lng": 1.3621168589428738
                    }]
                }, {
                    "color": "#1396A5",
                    "points": [{
                        "lat": 43.53780594519009,
                        "lng": 1.3620131703009255
                    }, {
                        "lat": 43.53871927987642,
                        "lng": 1.3623119912359811
                    }]
                }, {
                    "color": "#1396A5",
                    "points": [{
                        "lat": 43.53983270497822,
                        "lng": 1.359734151820824
                    }, {
                        "lat": 43.53980176586632,
                        "lng": 1.3598995964572302,
                        "station": "Easymile departure"
                    }]
                }, ]
            };

            var viewModel = v.toViewModel(json);


            expect(viewModel.center).toEqual([43.539698130087686,
                1.3620845608763283
            ]);
            expect(viewModel.lines[0]).toEqual({
                color: "#1396A5",
                points: [
                    [43.537571739831066,
                        1.36291393215029
                    ],
                    [43.537711013769666,
                        1.3621168589428738
                    ]
                ]
            });
            expect(viewModel.stations[0]).toEqual({
                "name": "Easymile departure",
                "position": [43.53980176586632, 1.3598995964572302]
            });
        });

    });

})(window.View);
