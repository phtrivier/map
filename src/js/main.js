(function(L, Model, View) {

    var handleStationClicked = function(station) {
        alert("Station " + station.name + " clicked");
    };

    var render = function(model){
        var view = new View({
            domId : "map",
            stationClicked : handleStationClicked
        });
        view.render(model);
    };

    Model.load().then(render);

})(window.L, window.Model, window.View);
