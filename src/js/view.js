(function(window, L) {

    /**
     * Francazal site view.
     *
     * @param props {Object}
     * @param props.domId {string} id of the DOM element to which the Leafleat Map should be attached
     */
    function View(props) {
        this.domId = props.domId;
        this.stationClicked = props.stationClicked;
    }

    /**
     * @typedef Position
     * @type Array
     * Latitude / Longitude array, as Number
     */

    /**
     * @typedef Station
     * @type Object
     * @property {string} name
     * @property {Position} position
     */

    /**
     * @typedef Line
     * @type Object
     * @property {string} color
     * @property {Array{Position}} points
     */

    /**
     * @typedef ViewModel
     * @type Object
     * @property {Position} center
     * @property {Array{Line}} lines
     * @property {Array{Station}} stations
     */

    /**
     * Convert the JSON site model to this View's model.
     *
     * @param model {Object} the JSON site model
     * @return ViewModel
     */
    View.prototype.toViewModel = function(model) {

        var viewModel = {
            center : extractLatLng(model.Map.origin),
            lines : [],
            stations : []
        };

        viewModel = model.Lines.reduce(function(accu, line) {

            var viewLine = {
                color : line.color,
                points : []
            };

            line.points.forEach(function(point) {

                var latlng = extractLatLng(point);
                viewLine.points.push(latlng);

                if (point.station) {
                    var viewStation = {
                        name : point.station,
                        position : latlng
                    };
                    accu.stations.push(viewStation);
                }
            });

            accu.lines.push(viewLine);

            return accu;

        }, viewModel);

        return viewModel;

    };

    /**
     * Naïve implementation only suitable for single rendering
     *
     * @param model {Object} the JSON site's model
     */
    View.prototype.render = function(model) {

        var viewModel = this.toViewModel(model);

        this.map = L.map(this.domId).setView(viewModel.center, 15);

        L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v10/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
            maxZoom : 18,
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            accessToken : 'pk.eyJ1IjoicGh0cml2aWVyMSIsImEiOiJjaXd6ZGdncWUwMDhiMnprMzZsdHhyY2IxIn0.wLTvtCMe8DL7tUGFy2vP3A'
        }).addTo(this.map);

        this.renderLines(viewModel.lines);
        this.renderStations(viewModel.stations);
    };

    View.prototype.renderLines = function(lines) {
        var self = this;
        lines.forEach(function (line) {
            L.polyline(line.points, { color : line.color}).addTo(self.map);
        });
    };

    View.prototype.renderStations = function(stations) {
        var self = this;
        stations.forEach(function (station) {
            var marker = L.marker(station.position, { title : station.name}).addTo(self.map);
            marker.on("click", function() {
                if (self.stationClicked) {
                    self.stationClicked(station);
                }
            });
        });
    };

    /**
     * Convert an object to a Lat / Lng Array
     *
     * @param point {Object}
     * @param point.lat {Number}
     * @param point.lng {Number}
     */
    function extractLatLng(point) {
        return [point.lat, point.lng];
    }

    window.View = View;

})(window, window.L);
