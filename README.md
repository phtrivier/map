This is my first attempt at representing the Map.
It is purely in Vanilla JS, and the model is hardcoded on the client side.

I have not done much error handling or validation.

This is a standalone index.html file (there is no server for the moment.)

## 'Architecture'.

I'm not using a framework, but I separated 'model' / 'view', and 'main' that acts as a controller.

## Dependency loading

I'm using a "poor-man"'s module system instead of requirejs or browserify.
I'm just loading all scripts and assuming they 'pollute' the main namespace for the moment.

## Model

I used https://github.com/stefanpenner/es6-promise to ensure ES6 Promise are available.
I use them in model.js to 'pretend' that the model is asynchronously read before the first rendering.
(But for the moment, the json is hardcoded on the client side.)

## View

I'm using leaflet.js, (I'm loading the library from the site directly.)
A render method in View recreates the Map at every display.
There are so examples of jsdoc for the inner types (I don't know if you use it.)

## Tests

View has code to convert from the JSON model to what leafleat.js needs ; I wrote a Jasmine spec
for this, as an example, that can be run by visiting "test.html".
Again, this does not use a module framework at all.
